import React from 'react';
import PropTypes from 'prop-types';

import Table from 'table';

import './ComponentInfo.scss';

/**
 * ComponentInfo
 * @description [Description]
 * @example
  <div id="ComponentInfo"></div>
  <script>
    ReactDOM.render(React.createElement(Components.ComponentInfo, {
    	title : 'Example ComponentInfo'
    }), document.getElementById("ComponentInfo"));
  </script>
 */
const ComponentInfo = props => {
	const baseClass = 'debug';

	// TODO

	const {componentDefaults, isTabular, ...rest} = props;

	if (!isTabular) {
		return (
			<div className={`${baseClass}`}>
				<pre className={`${baseClass}`}>{JSON.stringify(rest, null, 2)}</pre>
			</div>
		);
	}

	const tableRows = [];

	Object.entries(rest).forEach(([key, value]) => {
		const character = key.charAt(0);
		const isUpperCase = character === character.toUpperCase();
		const isWarning = componentDefaults !== null && componentDefaults[key] === undefined;
		const type = Array.isArray(value) ? 'array' : typeof value;

		tableRows.push(
			`<tr${isUpperCase || isWarning ? ' class="warn"' : ''}><th>${key}</th><td><pre>${JSON.stringify(
				value,
				null,
				'\t'
			)}</pre></td><td>${type}</td></tr>`
		);
	});

	return (
		<div className={`${baseClass}`}>
			<Table body={tableRows.join('')} />
		</div>
	);
};

ComponentInfo.defaultProps = {
	isTabular: false,
	componentDefaults: null
};

ComponentInfo.propTypes = {
	componentDefaults: PropTypes.object
};

export default ComponentInfo;
